package com.hunt.weatherreport.services;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hunt.weatherreport.models.User;

@Service
public class MockDataService {
	@Autowired
	UserService userService;
	
	@Autowired
	PasswordEncoder encoder;

	@PostConstruct
	public void users() {
		userService.deleteAll();
		
		User user = new User();
		user.setEmail("jim@ebox.edu");
		user.setUsername("jim");
		user.setPassword( encoder.encode("123") );
		user.setPhone("6087856822");		
		userService.save( user );
		
		user = new User();
		user.setEmail("jill@ebox.edu");
		user.setUsername("jill");
		user.setPassword( encoder.encode("123") );		
		user.setPhone("6087856822");		
		userService.save( user );
	}
	
}

